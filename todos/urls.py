from django.urls import path
from todos.views import todo_list, todo_detail, todo_create

urlpatterns = [
    path("create/", todo_create, name="todo_create"),
    path("<int:id>", todo_detail, name="todo_detail"),
    path("", todo_list, name="todo-list"),
]
