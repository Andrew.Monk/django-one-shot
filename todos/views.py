from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms  import TodoListForm


# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        'todo_lists': todo_lists,
    }
    return render(request, 'todos/list.html', context)


def todo_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        'todo_detail': details,
    }
    return render(request, 'todos/details.html', context)


def todo_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
           todo_list = form.save()
           return redirect('todo_detail', id=todo_list.id)
    else:
        form = TodoListForm()
        context = {
            'form': form,
        }
    return render(request, 'todos/create.html', context)
